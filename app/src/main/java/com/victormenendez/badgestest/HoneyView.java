package com.victormenendez.badgestest;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by victormenendez on 30/06/16.
 */
public class HoneyView extends ViewGroup {


	private final float MAX_SCALE = 2.5f;
	private final float MIN_SCALE = 0;
	private final float SCALE_TO_DISSAPEAR = 0.25f; //Value of the scale where the alpha will be 0
	private final int ELEMENTS_PER_ROW = 6;
	private final int screenWidth;
	private final int screenHeight;
	private float _xDelta;
	private float _yDelta;
	private float centerX;
	private float centerY;
	private int padding;


	public HoneyView(Context context, int screenWidth, int screenHeight) {
		super(context);
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		setClipChildren(false);
	}


	@Override
	// Method that establishes position of every child view
	protected void onLayout(boolean changed, int l, int t, int r, int b) {


		centerX = (screenWidth / 2) - t;
		centerY = (screenHeight / 2) - t;


		int itemWidth = HexagonView.HEXAGON_DEFAULT_SIZE;
		int row = 0;
		int column = 0;
		int gapX;
		int gapY;


		// Padding added to parent view, to not cut child views in case on max scale
		padding = (int)MAX_SCALE * (itemWidth / 2 + HexagonView.HEXAGON_MARGIN / 2);


		// Organize child views
		for (int i = 0; i < this.getChildCount(); i++) {
			View v = getChildAt(i);


			// Gaps are to translate views on even rows, to make them fit in each other, like a honeycomb
			gapX = 0;
			if (row % 2 != 0) {
				gapX = HexagonView.GAP_X;
			}


			gapY = HexagonView.GAP_Y * row;


			int left = padding + ((itemWidth * column) + gapX + HexagonView.HEXAGON_MARGIN / 2);
			int top = padding + ((itemWidth * row) - gapY + HexagonView.HEXAGON_MARGIN / 2);
			int right = padding + (((column + 1) * itemWidth) + gapX - HexagonView.HEXAGON_MARGIN / 2);
			int bottom = padding + (((row + 1) * itemWidth) - gapY - HexagonView.HEXAGON_MARGIN / 2);


			v.layout(left, top, right, bottom);


			column++;
			if (column >= ELEMENTS_PER_ROW) {
				row++;
				column = 0;
			}


		}
	}


	@Override
	// Method called when view is created to know how much it is going to measure
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


		int desiredWidth = 2 * padding + (HexagonView.HEXAGON_DEFAULT_SIZE) * ELEMENTS_PER_ROW + HexagonView.GAP_X;
		int desiredHeight =
				2 * padding + ((HexagonView.HEXAGON_DEFAULT_SIZE - HexagonView.GAP_Y) * (int)Math.ceil(getChildCount() / ELEMENTS_PER_ROW)) + HexagonView.HEXAGON_DEFAULT_SIZE / 2
						- HexagonView.GAP_Y;


		int width = MeasureSpec.makeMeasureSpec(desiredWidth, MeasureSpec.EXACTLY);
		int height = MeasureSpec.makeMeasureSpec(desiredHeight, MeasureSpec.EXACTLY);


		super.onMeasure(width, height);
	}


	@Override
	// Catching touch event
	public boolean onTouchEvent(MotionEvent event) {


		final int X = (int)event.getRawX();
		final int Y = (int)event.getRawY();


		switch (event.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				_xDelta = X - getX();
				_yDelta = Y - getY();
				break;
			case MotionEvent.ACTION_UP:
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				break;
			case MotionEvent.ACTION_POINTER_UP:
				break;
			case MotionEvent.ACTION_MOVE:
				float x = X - _xDelta;
				float y = Y - _yDelta;


				if (y < (HexagonView.HEXAGON_DEFAULT_SIZE / 2 + padding) && y > -getHeight() + centerY) {
					setY(y);
				}


				if (x < (HexagonView.HEXAGON_DEFAULT_SIZE / 2 + padding - HexagonView.GAP_X) && x > -getWidth() + centerX + padding + HexagonView.GAP_X) {
					setX(x);
				}


				break;
		}
		invalidate();
		return true;


	}


	@Override
	protected void onDraw(Canvas canvas) {
		escalate();
	}


	// Method that change size of views, depending on how close they are to the center
	public void escalate() {


		for (int i = 0; i < this.getChildCount(); i++) {
			View view = getChildAt(i);


			HexagonView v = (HexagonView)view;


			int test1[] = new int[2];
			v.getLocationInWindow(test1);


			float centerPointX = test1[0] + ((v.getWidth() / 2) * v.getScaleX());
			float centerPointY = test1[1] + ((v.getHeight() / 2) * v.getScaleY());
			float dist = (float)Math.sqrt((centerX - centerPointX) * (centerX - centerPointX) + (centerY - centerPointY) * (centerY - centerPointY));


			float scale = 0;
			if (dist < getHeight() / 2) {


				float d = getHeight() / 2;
				float s = MAX_SCALE;
				float x = dist;
				float sqrt = (float)Math.sqrt(2 * Math.pow(d, 3) * Math.pow(s, 2) * x - Math.pow(d, 2) * Math.pow(s, 2) * Math.pow(x, 2));
				float up = (float)(Math.pow(d, 2) * s - sqrt);
				scale = (float)(up / Math.pow(d, 2));


				v.setScaleY(scale);
				v.setScaleX(scale);


				float alpha = 0;
				if (scale > SCALE_TO_DISSAPEAR) {
					if (scale > 1) {
						alpha = 1;
					} else {
						alpha = (float)Math.sqrt(1 - 1 / Math.pow(1 - SCALE_TO_DISSAPEAR, 2) + (2 * scale) / Math.pow(1 - SCALE_TO_DISSAPEAR, 2) - Math.pow(scale, 2) / Math
								.pow(1 - SCALE_TO_DISSAPEAR, 2));
					}
				}
				v.setAlpha(alpha);
			}


			move(v, i);
		}


	}


	// Method that moves views next to the ones that are escalated, depending on the new size
	// JUST MOVING LEFT AND RIGHT VIEW SO FAR
	public void move(View closestView, int position) {


		int row = position / ELEMENTS_PER_ROW;
		int column = position - (ELEMENTS_PER_ROW * row);


		if (column < 0) {
			column = 0;
		}


		View top_right, bottom_left, left = null, right = null, top_left, bottom_right, twoLeft = null;


		if (column < ELEMENTS_PER_ROW - 1) {
			right = getChildAt(position + 1);
		}


		if (column > 0) {
			left = getChildAt(position - 1);
			if (column == 1) {
				twoLeft = null;
			} else {
				twoLeft = getChildAt(position - 2);
			}


		}


		if (row % 2 != 0) {
			top_right = getChildAt(position - ELEMENTS_PER_ROW + 1);
			top_left = getChildAt(position - ELEMENTS_PER_ROW);
			bottom_right = getChildAt(position + ELEMENTS_PER_ROW + 1);
			bottom_left = getChildAt(position + ELEMENTS_PER_ROW);
		} else {
			top_right = getChildAt(position - ELEMENTS_PER_ROW);
			top_left = getChildAt(position - ELEMENTS_PER_ROW - 1);
			bottom_right = getChildAt(position + ELEMENTS_PER_ROW);
			bottom_left = getChildAt(position + ELEMENTS_PER_ROW - 1);
		}


		if (right != null) {
			if (closestView.getScaleX() >= 1) {
				right.setTranslationX((closestView.getScaleX() - 1) * HexagonView.HEXAGON_DEFAULT_SIZE / 2);
			} else {
				right.setTranslationX(0);
			}
		}


		if (left != null) {
			if (twoLeft == null || closestView.getScaleX() >= twoLeft.getScaleX()) {
				if (closestView.getScaleX() >= 1) {
					left.setTranslationX((1 - closestView.getScaleX()) * HexagonView.HEXAGON_DEFAULT_SIZE / 2);
				} else {
					left.setTranslationX(0);
				}


			}
		}


		if (top_right != null) {
			if (right == null || closestView.getScaleY() >= right.getScaleY()) {
				if (closestView.getScaleX() >= 1) {
					top_right.setTranslationY(-(closestView.getScaleY() - 1) * HexagonView.HEXAGON_DEFAULT_SIZE / 2);
//					if (top_left.getScaleX() < closestView.getScaleX()) {
//						top_right.setTranslationX((closestView.getScaleX() - 1) * HexagonView.HEXAGON_DEFAULT_SIZE / 2);
//					}
				}
			}
		}


		if (top_left != null) {
			if (left == null || closestView.getScaleY() >= left.getScaleY()) {
				if (closestView.getScaleX() >= 1) {
					top_left.setTranslationY(-(closestView.getScaleY() - 1) * HexagonView.HEXAGON_DEFAULT_SIZE / 2);
//						if (top_right.getScaleX() < closestView.getScaleX()) {
//							top_left.setTranslationX((1 - closestView.getScaleX()) * HexagonView.HEXAGON_DEFAULT_SIZE / 2);
//						}
				}
			}
		}


		if (bottom_right != null) {
			if (right == null || closestView.getScaleY() >= right.getScaleY()) {
				if (closestView.getScaleX() >= 1) {
					bottom_right.setTranslationY((closestView.getScaleY() - 1) * HexagonView.HEXAGON_DEFAULT_SIZE / 2);
//					if (top_left.getScaleX() < closestView.getScaleX()) {
//						top_right.setTranslationX((closestView.getScaleX() - 1) * HexagonView.HEXAGON_DEFAULT_SIZE / 2);
//					}
				}
			}
		}

		if (bottom_left != null) {
			if (left == null || closestView.getScaleY() >= left.getScaleY()) {
				if (closestView.getScaleX() >= 1) {
					bottom_left.setTranslationY((closestView.getScaleY() - 1) * HexagonView.HEXAGON_DEFAULT_SIZE / 2);
				}
			}
		}


	}
}
