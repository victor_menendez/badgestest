package com.victormenendez.badgestest;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.widget.RelativeLayout;

public class MainActivity extends Activity {

	private Bitmap mBitmap;
	private Canvas mCanvas;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;

		setContentView(R.layout.activity_main);
		final RelativeLayout relative = (RelativeLayout)findViewById(R.id.relative);

		HoneyView honeyView = new HoneyView(this, width, height);
		honeyView.setWillNotDraw(false);
		relative.addView(honeyView);

		HexagonView hexagonView;
		for (int i = 0; i <= 59; i++) {
			hexagonView = new HexagonView(this);
			honeyView.addView(hexagonView);
		}
	}

}
