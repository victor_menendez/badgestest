package com.victormenendez.badgestest;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.View;

/**
 * Created by victormenendez on 30/06/16.
 */
public class HexagonView extends View {

	public static int HEXAGON_DEFAULT_SIZE = 250;
	public static int HEXAGON_MARGIN = 30;
	public static int GAP_X = HEXAGON_DEFAULT_SIZE / 2;
	public static int GAP_Y = (int)(0.4514 * (HEXAGON_DEFAULT_SIZE / 2));

	private final Bitmap hexagon;
	private final Bitmap iconImage;
	private float scaleX;
	private float scaleY;

	public HexagonView(Context context) {
		super(context);

		Bitmap original = Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.hexagon));
		hexagon = Bitmap.createScaledBitmap(original, HEXAGON_DEFAULT_SIZE - HEXAGON_MARGIN, HEXAGON_DEFAULT_SIZE - HEXAGON_MARGIN, true);

		TypedArray images = getResources().obtainTypedArray(R.array.loading_images);
		int random = (int) (Math.random() * images.length());
		int icon = images.getResourceId(random, R.drawable.image_01);
		Bitmap iconOriginal = Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), icon));
		iconImage = Bitmap.createScaledBitmap(iconOriginal, (HEXAGON_DEFAULT_SIZE - HEXAGON_MARGIN)/2, (HEXAGON_DEFAULT_SIZE - HEXAGON_MARGIN)/2, true);

		scaleX = 1;
		scaleY = 1;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawBitmap(hexagon, 0, 0, null);
		canvas.drawBitmap(iconImage, (HEXAGON_DEFAULT_SIZE - HEXAGON_MARGIN)/2 - iconImage.getWidth()/2, (HEXAGON_DEFAULT_SIZE - HEXAGON_MARGIN)/2 - iconImage.getHeight()/2, null);
		//postInvalidateDelayed(20);
	}

	@Override
	public void setScaleX(float scaleX) {
		super.setScaleX(scaleX);
		this.scaleX = scaleX;
	}

	@Override
	public void setScaleY(float scaleY) {
		super.setScaleY(scaleY);
		this.scaleY = scaleY;
	}

	@Override
	public float getScaleX() {
		return scaleX;
	}

	@Override
	public float getScaleY() {
		return scaleY;
	}

}
